package groovy.measure.application;

import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.router.Route;

@Route("")
public class MainView extends VerticalLayout {

    public MainView() {
        Button b = new Button("Klicka");
        add(b);
        b.addClickListener(e -> Notification.show("Klick", 10000, Notification.Position.MIDDLE));
    }

}
